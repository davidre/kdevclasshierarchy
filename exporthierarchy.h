/*
 * Copyright 2019 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef EXPORT_CLASS_HIERACHY_H
#define EXPORT_CLASS_HIERACHY_H

#include <QColor>
#include <QMap>
#include <QString>

class QTemporaryFile;
class QPalette;

namespace KDevelop {
    class Declaration;
}

class HierarchyExporter {
public:
    QTemporaryFile* writeClassHierarchy(const KDevelop::Declaration* declaration, const QPalette& palette,
                                        QMap<QString, const KDevelop::Declaration*>& declarationMap);
private:
    QColor colorForDeclaration(const KDevelop::Declaration* declaration, bool background) const;
    void writeClass(const KDevelop::Declaration* declaration, QMap<QString,
                    const KDevelop::Declaration*>& declarationMap) const;
    void writeEdge(const KDevelop::Declaration* base, const KDevelop::Declaration* derived) const;
    void writeDerivedClasses(const KDevelop::Declaration* declaration,
                             QMap<QString, const KDevelop::Declaration*>& declarationMap) const;
    void writeBaseClasses(const KDevelop::Declaration* declaration, 
                            QMap<QString, const KDevelop::Declaration*>& declarationMap) const;

    const QPalette* m_palette;
    QTemporaryFile* m_currentFile;
};
#endif
