/*
 * Copyright 2019 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CLASSHIERACHYVIEW_H
#define CLASSHIERACHYVIEW_H

#include "exporthierarchy.h"

#include <interfaces/iuicontroller.h>
#include <language/classmodel/classmodel.h>

#include <QString>

class QFile;
class QStackedLayout;

class KPluginFactory;

namespace KGraphViewer {
class KGraphViewerInterface;
}

namespace KParts {
class ReadOnlyPart;
}

namespace KDevelop {
class ClassDeclaration;
}

class ClassHierarchyPlugin;

class ClassHierarchyViewFactory : public KDevelop::IToolViewFactory {
public:
    ClassHierarchyViewFactory (KPluginFactory* partFactory, ClassHierarchyPlugin* plugin);
    QWidget* create(QWidget* parent = nullptr) override;
    QString id() const override;
    Qt::DockWidgetArea defaultPosition() override;
private:
    ClassHierarchyPlugin* m_plugin;
    KPluginFactory* m_partFactory;
};
class ClassHierarchyView : public QWidget {
    Q_OBJECT
public:
    explicit ClassHierarchyView(KPluginFactory* partFactory, ClassHierarchyPlugin* plugin, QWidget* = nullptr);
    void showClassHierarchy(KDevelop::DeclarationPointer declaration);

private Q_SLOTS:
    void elementHovered(const QString& id);
    void hoverLeave(const QString&);
    void nodeClicked(const QList<QString>& ids, const QPoint&);

private:
    QString m_currentClass;
    QFile* m_currentFile;
    QMap<QString, const KDevelop::Declaration*> m_declarationMap;
    QStackedLayout* m_layout;
    HierarchyExporter m_exporter;
    ClassHierarchyPlugin* m_plugin;
    KParts::ReadOnlyPart* m_part;
    KGraphViewer::KGraphViewerInterface* m_graphInterface;
};

#endif
