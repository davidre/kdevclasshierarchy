/*
 * Copyright 2019 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "exporthierarchy.h"

#include "debug.h"

#include <interfaces/icore.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/declaration.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/types/structuretype.h>
#include <util/path.h>
#include <util/widgetcolorizer.h>

#include <QTemporaryFile>

#include <numeric>

using namespace KDevelop;

QColor HierarchyExporter::colorForDeclaration(const Declaration* declaration, bool background) const
{
    const auto project = ICore::self()->projectController()->findProjectForUrl(declaration->url().toUrl());
    QColor color;
    if (project) {
        qCDebug(PLUGIN_CLASSHIERARCHY) << declaration->abstractType()->toString() << " is in project" << project->name();
        color = WidgetColorizer::colorForId(qHash(project->path()), *m_palette, background);
    } else {
        qCDebug(PLUGIN_CLASSHIERARCHY) << "no project for " << declaration->abstractType()->toString();
        color = background ? Qt::GlobalColor::white : Qt::GlobalColor::black;
    }
    qCDebug(PLUGIN_CLASSHIERARCHY) << "coloring with " << color;
    return color;
}

void HierarchyExporter::writeClass(const Declaration* declaration,
                                  QMap<QString, const Declaration*>& declarationMap) const
{
        if (!declaration->abstractType()) {
            qDebug() << declaration->abstractType().data();
        }
        const QString className = declaration->abstractType()->toString();
        const QString backgroundColor = colorForDeclaration(declaration, true).name();
        const QString borderColor = colorForDeclaration(declaration, false).name();
        m_currentFile->write(QStringLiteral("\"%1\"[fillcolor=\"%2\", color=\"%3\"];\n").arg(className,
                                backgroundColor, borderColor).toUtf8());
        declarationMap[className] = declaration;
}

void HierarchyExporter::writeEdge(const Declaration* base, const Declaration* derived) const
{
    const QString derivedName = derived->abstractType()->toString();
    const QString baseName = base->abstractType()->toString();
    m_currentFile->write(QStringLiteral("\"%1\" -> \"%2\";\n").arg(derivedName, baseName).toUtf8());
}

void HierarchyExporter::writeDerivedClasses(const Declaration* declaration,
                                            QMap<QString, const Declaration*>& declarationMap) const
{
  uint max = std::numeric_limits<uint>::max();
    const QList<Declaration*> derivedClasses = DUChainUtils::inheriters(declaration, max);
    for (const auto derived : derivedClasses) {
        writeClass(derived, declarationMap);
        writeEdge(declaration, derived);
        auto derivedClass = dynamic_cast<ClassDeclaration*>(derived);
        if (derivedClass) {
            writeDerivedClasses(derivedClass, declarationMap);
        }
    }
}

void HierarchyExporter::writeBaseClasses(const Declaration* declaration,
                                        QMap<QString, const Declaration*>& declarationMap) const
{
    const ClassDeclaration* classDeclaration = dynamic_cast<const ClassDeclaration*>(declaration);
    if (!classDeclaration) {
        return;
    }
    const BaseClassInstance* baseClasses = classDeclaration->baseClasses();
    qCDebug(PLUGIN_CLASSHIERARCHY) << "Writing baseclasses for " << declaration->abstractType()->toString();
    for (uint i = 0; i < classDeclaration->baseClassesSize(); ++i) {
        qCDebug(PLUGIN_CLASSHIERARCHY) << declaration->abstractType()->toString() << ":" << baseClasses[i].baseClass.abstractType()->toString();
        auto structureType = baseClasses[i].baseClass.type<StructureType>();
//         if (structureType) {
        auto baseDeclaration = structureType->declaration(declaration->topContext());
        if (baseDeclaration) {
            writeClass(baseDeclaration, declarationMap);
            writeEdge(baseDeclaration, declaration);
            auto baseClass = dynamic_cast<ClassDeclaration*>(baseDeclaration);
            if (baseClass) {
                writeBaseClasses(baseClass, declarationMap);
            }
        } else { //fallback write at least manually
            qCDebug(PLUGIN_CLASSHIERARCHY) << "fallback";
            const QString derivedName = declaration->abstractType()->toString();
            m_currentFile->write(QStringLiteral("%1 -> %2;\n").arg(derivedName, structureType->toString()).toUtf8());
        }
    }
}

QTemporaryFile* HierarchyExporter::writeClassHierarchy(const Declaration* declaration, const QPalette& palette, 
                                                     QMap<QString, const Declaration*>& declarationMap)
{
    m_palette = &palette;
    m_currentFile =  new QTemporaryFile;
    m_currentFile->open();
    qCDebug(PLUGIN_CLASSHIERARCHY) << "Writing to " << m_currentFile->fileName();
    m_currentFile->write("strict digraph {\nrankdir=\"BT\"\n"
        "edge[arrowhead=\"empty\"];\n"
        "node[shape=\"box\"]");
    writeClass(declaration, declarationMap);
    writeDerivedClasses(declaration, declarationMap);
    writeBaseClasses(declaration, declarationMap);
    m_currentFile->write("}");
    return m_currentFile;
}
