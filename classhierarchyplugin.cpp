/*
 * Copyright 2019 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "classhierarchyplugin.h"

#include "classhierarchyview.h"
#include "debug.h"

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/interfaces/codecontext.h>

#include <KLocalizedString>
#include <KPluginFactory>
#include <KPluginLoader>
#include <KService>

#include <QAction>

using namespace KDevelop;

K_PLUGIN_CLASS_WITH_JSON(ClassHierarchyPlugin,"kdevclasshierarchy.json")

ClassHierarchyPlugin::ClassHierarchyPlugin(QObject* parent, const QVariantList&)
    : IPlugin{QStringLiteral("kdevclasshierarchy"), parent}
    , m_showClassHierarchy{new QAction(i18n("Show Class Hierarchy"), this)}
{
    KPluginFactory* factory;
    KService::Ptr service = KService::serviceByDesktopName(QStringLiteral("kgraphviewerpart"));
    if (service) {
        factory = KPluginLoader(*service).factory();
    } else {
         factory = KPluginLoader(QStringLiteral("kgraphviewerpart")).factory();
    }
    if (factory) {
        core()->uiController()->addToolView(i18n("Class Hierarchy"), new ClassHierarchyViewFactory (factory, this));
        connect(m_showClassHierarchy, &QAction::triggered, this, [this] {
            m_view->showClassHierarchy(m_lastContextDeclaration);
        });
    } else {
        qCWarning(PLUGIN_CLASSHIERARCHY) << "Couldn't load kgraphviewerpart";
    }
}

void ClassHierarchyPlugin::setView(ClassHierarchyView* view)
{
    m_view = view;
}

ContextMenuExtension ClassHierarchyPlugin::contextMenuExtension(Context* context, QWidget* parent)
{
    DUChainReadLocker readLock(DUChain::lock());
    ContextMenuExtension menuExt = IPlugin::contextMenuExtension(context, parent);
    auto declaration = classDeclaration (context);
    if (m_view != nullptr && declaration) {
        m_lastContextDeclaration = DeclarationPointer(declaration);
        menuExt.addAction(ContextMenuExtension::ExtensionGroup, m_showClassHierarchy);
    }
  return menuExt;
}

KDevelop::ClassDeclaration* ClassHierarchyPlugin::classDeclaration(KDevelop::Context* context)
{
    auto* codeContext = dynamic_cast<DeclarationContext*>(context);
    if (!codeContext) {
        return nullptr;
    }
    Declaration* decl(codeContext->declaration().data());
    //if copied from classbrowserplugin.cpp
    if (decl) {
        qCDebug(PLUGIN_CLASSHIERARCHY) << decl->kind();
        if (decl->inSymbolTable()) {
            qCDebug(PLUGIN_CLASSHIERARCHY) << "Is forward?" << decl->isForwardDeclaration();
            if (decl->kind() == Declaration::Type && decl->internalContext()) {
                qCDebug(PLUGIN_CLASSHIERARCHY) << decl->internalContext()->type();
                if( decl->internalContext()->type() == DUContext::Class) {
                    return dynamic_cast<ClassDeclaration*>(decl);
                }
            }
        }
        //TODO test typedef/forward declaration?
    }
    return nullptr;
}

#include "classhierarchyplugin.moc"
