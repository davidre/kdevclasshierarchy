/*
 * Copyright 2019 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "classhierarchyview.h"

#include "debug.h"
#include "classhierarchyplugin.h"
#include "exporthierarchy.h"

#include "kgraphviewer_interface.h"

#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <language/duchain/classdeclaration.h>

#include <KParts/ReadOnlyPart>
#include <KPluginFactory>

#include <QAbstractScrollArea>
#include <QStackedLayout>
#include <QTemporaryFile>
#include <QTimer>
#include <QUrl>

using namespace KDevelop;

ClassHierarchyViewFactory::ClassHierarchyViewFactory(KPluginFactory* partFactory, ClassHierarchyPlugin* plugin)
    : m_plugin{plugin}
    , m_partFactory{partFactory}
{
}

QWidget * ClassHierarchyViewFactory::create(QWidget* parent)
{
    return new ClassHierarchyView(m_partFactory, m_plugin, parent);
}

QString ClassHierarchyViewFactory::id() const
{
    return QStringLiteral("org.kdevelop.ClassHierarchyView");
}

Qt::DockWidgetArea ClassHierarchyViewFactory::defaultPosition()
{
    return Qt::BottomDockWidgetArea;
}

ClassHierarchyView::ClassHierarchyView(KPluginFactory* partFactory, ClassHierarchyPlugin* plugin, QWidget* parent)
    : QWidget{parent}
    , m_plugin{plugin}
{
    m_plugin->setView(this);
    m_layout = new QStackedLayout(this);
    m_layout->addWidget(new QWidget(this));
    m_part = partFactory->create<KParts::ReadOnlyPart>(this);
    static_cast<QAbstractScrollArea*>(m_part->widget())->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    static_cast<QAbstractScrollArea*>(m_part->widget())->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    if (m_part) {
        qCDebug(PLUGIN_CLASSHIERARCHY) << "Created part";
        m_graphInterface = qobject_cast<KGraphViewer::KGraphViewerInterface*>(m_part);
        // use String connect syntax since SelectionIs is only declared in kgraphviewer_part.h
        connect(m_part, SIGNAL(selectionIs(QList<QString>, QPoint)),
                this, SLOT(nodeClicked(const QList<QString>&, const QPoint&)));
        // setReadWrite for selection
        QMetaObject::invokeMethod(m_part, "setReadWrite");
        m_graphInterface->slotUnsetCursor();
        m_layout->addWidget(m_part->widget());
        connect(m_part, SIGNAL(hoverEnter(const QString&)), this, SLOT(elementHovered(const QString&)));
        connect(m_part, SIGNAL(hoverLeave(const QString&)), this, SLOT(hoverLeave(const QString&)));
        connect(m_part, QOverload<>::of(&KParts::ReadOnlyPart::completed), this, [this] {
            m_layout->setCurrentIndex(1);
            ICore::self()->uiController()->raiseToolView(this);
        });
    } else {
        qCDebug(PLUGIN_CLASSHIERARCHY) << "Couldn't create part";
    }
}

void ClassHierarchyView::elementHovered(const QString& id)
{
    // Otherwise it's an edge
    if (m_declarationMap.contains(id)) {
        setCursor(Qt::PointingHandCursor);
    }
}

void ClassHierarchyView::hoverLeave(const QString&)
{
    unsetCursor();
}

void ClassHierarchyView::showClassHierarchy(DeclarationPointer declaration)
{
    DUChainReadLocker readLock(DUChain::lock());
    const QString className = declaration->abstractType()->toString();
    if (m_currentClass == className) {
        ICore::self()->uiController()->raiseToolView(this);
        return;
    }
    m_currentClass = className;
    m_currentFile = m_exporter.writeClassHierarchy(declaration.data(), palette(), m_declarationMap);
    m_currentFile->flush();
    m_part->openUrl(QUrl::fromLocalFile(m_currentFile->fileName()));

}

void ClassHierarchyView::nodeClicked(const QList<QString>& ids, const QPoint& point)
{
    qCDebug(PLUGIN_CLASSHIERARCHY) << ids << " clicked at " << point;
    qCDebug(PLUGIN_CLASSHIERARCHY) << m_currentClass << " is current";
    DUChainReadLocker readLock(DUChain::lock());
    auto declaration = dynamic_cast<const KDevelop::ClassDeclaration*>(m_declarationMap[ids[0]]);
    if (declaration) {
        if (m_currentClass != ids[0]) {
            m_currentClass = ids[0];
            m_layout->setCurrentIndex(0);
            m_currentFile->deleteLater();
            m_currentFile = m_exporter.writeClassHierarchy(declaration, palette(), m_declarationMap);
            m_currentFile->flush();
//             m_part->closeUrl();
//             m_part->openUrl(QUrl::fromLocalFile(m_currentFile->fileName()));
            // If we open directly without waiting we crash inside QGraphicsItemPrivate
            QTimer::singleShot(200, [this]{m_part->openUrl(QUrl::fromLocalFile(m_currentFile->fileName()));});
        }
        QUrl url = declaration->url().toUrl();
        auto cursor = declaration->range().start;
        int line = cursor.line;
        int column = cursor.column;
        readLock.unlock();
        ICore::self()->documentController()->openDocument(url, KTextEditor::Cursor(line, column));
    }
}


